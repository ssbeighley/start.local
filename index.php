<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Hey There</title>
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/style.css" />
</head>
<body>
  <?php date_default_timezone_set("America/New_York"); ?>
  <div class="container">
    <header class="row">
      <div class="col-md-8">
        <h1>This is where everything begins...</h1>
      </div>
      <div class="col-md-4">
        <h2><?php echo date('h:i a');?> on <?php echo date('M d, Y');?></h2>
      </div>
    </header>
    <div class="row">
      <div class="col-md-8">
        <div class="panel panel-default">
          <div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><a href="http://www.getpocket.com"><strong>Articles Worth Reading</strong></a></div>
          <div class="panel-body">
            <div class="row">
              <div id="pocketFeed__wrap" class="col-xs-12"> <!-- RSS Feed Generated Here --> </div>
            </div>
          </div>
          <div class="panel-footer"> </div>
        </div> <!-- end feed --> 
      </div>
      <div class="col-md-4">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title"> <span class="glyphicon glyphicon-music"></span> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> Entertainment </a> </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <ul class="iconList">
                  <li><a href="http://www.facebook.com"><img class="icon" src="images/facebook-logo.png" alt="" /></a></li>
                  <li><a href="http://www.twitter.com"><img class="icon" src="images/twitter-logo.png" alt="" /></a></li>
                  <li><a href="http://www.pandora.com"><img class="icon" src="images/pandora-logo.png" alt=""/></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title"> <span class="glyphicon glyphicon-wrench"></span> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> Development </a> </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                <ul class="iconList">
                  <li><a href="https://www.google.com/analytics/web/?hl=en"><img class="icon" src="images/google_analytics.png" alt="" /></a> </li>
                  <li><a href="https://www.google.com/fonts"><img class="icon" src="images/fonts-128.png" alt=""/></a> </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
              <h4 class="panel-title"> <span class="glyphicon glyphicon-flash"></span> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> Performance </a> </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body">
                <ul class="iconList">
                  <li><a href="https://developers.google.com/speed/pagespeed/insights/"><img class="icon" src="images/pagespeed-logo.png" alt="" /></a> </li>
                  <li><a href="https://gtmetrix.com/" alt=""/><img class="icon" src="images/gtmetrix-logo.jpg" alt=""/></a> </li>
                  <li><a href="tools.pingdom.com/fpt/"><img class="icon" src="images/pingdom-logo.png" alt="" /></a> </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFour">
              <h4 class="panel-title"> <span class="glyphicon glyphicon-tasks"></span> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour"> Tools and Productivity </a> </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
              <div class="panel-body">
                <ul class="iconList">
                  <li><a href="http://drive.google.com"><img class="icon" src="images/drive-128.png" alt=""/></a> </li>
                  <li><a href="https://www.google.com/voice/b/0?pli=1#inbox"><img class="icon" src="images/Google_Voice_mirror.png" alt=""/></a> </li>
                  <li><a href="http://www.toodledo.com/tasks/index.php"><img class="icon" src="images/toodledo-logo.png" alt=""/></a> </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        
        <div class="panel panel-default">
          <div class="panel-heading"> <span class="glyphicon glyphicon-calendar"></span><b>Coming Up Soon</b></div>
          <div class="panel-body">
            <iframe src="https://www.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;mode=AGENDA&amp;height=400&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=ssb%40dozingdogdesign.com&amp;color=%23853104&amp;ctz=America%2FNew_York" style=" border-width:0 " width="100%" height="400" frameborder="0" scrolling="no"></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="js/jquery-1.11.3.min.js"></script> 
  <script src="js/bootstrap.min.js"></script> 
  <script src="js/moment.min.js"></script>
  <script src="js/jquery.rss.js"></script>
  <script src="js/appcode.js"></script>
</body>
</html>


