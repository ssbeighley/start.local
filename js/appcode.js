(function () {
  'use strict';

  $("#pocketFeed__wrap").rss(
	"https://getpocket.com/users/ssbeighley/feed/all",
	{
	  // how many entries do you want?
	  // default: 4
	  // valid values: any integer
	  limit: 10,
	  
	  // outer template for the html transformation
	  // default: "<ul>{entries}</ul>"
	  // valid values: any string
	  layoutTemplate: "<ul class='pocketFeed'>{entries}</ul>",
	  
	  // inner template for each entry
	  // default: '<li><a href="{url}">[{author}@{date}] {title}</a><br/>{shortBodyPlain}</li>'
	  // valid values: any string
	  entryTemplate: "<li class='pocketFeed__item'><span>{date}</span> <a href='{url}'>{title}</a></li>",
	  
	  // formats the date with moment.js (optional)
	  // default: 'dddd MMM Do'
	  // valid values: see http://momentjs.com/docs/#/displaying/
	  dateFormat: 'MM/DD/YY'
	}
  );
  
  
}());
